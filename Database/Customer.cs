﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThynkSoftwareTechnicalTask.Database
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Position { get; set; }

        [StringLength(50)]
        public string Hobbies { get; set; }

        [Required]
        [StringLength(50)]
        public string Hometown { get; set; }

        [StringLength(250)]
        public string Motto { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(50)]
        public string PersonalWebsite { get; set; }

        public byte[] Image { get; set; }
    }
}
