﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ThynkSoftwareTechnicalTask.Database;

/*
   "ConnectionStrings": {
    "SqlConnectionString": "Server=(localdb)\\MSSQLLocalDB;Initial Catalog=CustomersDb;Integrated Security=true"
  }
 */

namespace ThynkSoftwareTechnicalTask.Repositories
{
    public  class Repository<T> : IRepository<T> where T : Customer
    {
        protected readonly CustomerDbContext context;
        private DbSet<T> entities;
        string errorMessage = string.Empty;
        public  Repository(CustomerDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }
        public  IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }
        public  T GetById(int id)
        {
            return entities.SingleOrDefault(s => s.Id == id);
        }
        public  void Insert(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");

            entities.Add(entity);
            context.SaveChanges();
        }
        public void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void Delete(int id)
        {
            T entity = entities.SingleOrDefault(s => s.Id == id);
            entities.Remove(entity);
            context.SaveChanges();
        }
    }
}
