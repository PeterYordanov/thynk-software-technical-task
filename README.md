# Thynk Software Technical Task


[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/thynk-software-technical-task/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/calculator/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
  
<p>
<details>
<summary><h2>Screenshots</h2></summary>

![Start](Screenshots/Start.JPG "Start")
![Edit](Screenshots/Edit.JPG "Edit")
![Details](Screenshots/Details.JPG "Details")
![Delete](Screenshots/Delete.JPG "Delete")
![Add](Screenshots/Add.JPG "Add")
![Loading](Screenshots/Loading.JPG "Loading")
![Empty](Screenshots/Empty.JPG "Empty")

</details>
</p>

## Tech Stack
- React
- ASP.NET Core
- Entity Framework
- Swagger
- MSSQL

## Databaset Setup
From package manager console:
```bash
Install-Package Microsoft.EntityFrameworkCore.Tools
add-migration init
update-database
```

## Features
- [x] Add
- [x] Edit
- [x] Delete
- [x] View
- [x] List all
