import React, { Component } from 'react';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <label>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris bibendum risus ut lacus mollis tempus. Phasellus elementum fringilla arcu, eget maximus risus gravida tempor. Maecenas magna orci, ultricies in mattis a, convallis vitae nunc. Sed vitae varius sem. Ut placerat tincidunt turpis, ac volutpat velit efficitur vel. Donec et pellentesque elit. Aenean tincidunt sapien et fermentum eleifend. Vivamus bibendum neque mi, sit amet egestas purus semper eu. Praesent eleifend dolor diam, nec pulvinar orci commodo et. Duis vel ex ut est accumsan suscipit. Nam placerat lacinia augue, sit amet facilisis mi pretium a. Maecenas pulvinar vehicula fermentum. Sed sollicitudin vestibulum tellus quis sollicitudin. Cras nulla ipsum, tincidunt vitae aliquam et, volutpat sit amet lectus. Nulla placerat ipsum non massa posuere facilisis. Nam mollis arcu at venenatis convallis.
                    <br/>
                    Maecenas consectetur orci metus, quis placerat enim tempus non. Suspendisse maximus rhoncus ante, vel cursus nulla vulputate eget. Pellentesque nec tellus vitae dolor suscipit facilisis. Phasellus ut lorem vel elit pretium aliquam et sodales sapien. Integer non ante bibendum, egestas massa id, imperdiet odio. Nullam et ante leo. Vestibulum tincidunt dapibus diam quis elementum. Vivamus dignissim, eros id sagittis luctus, quam urna porttitor justo, vitae semper magna turpis nec lacus. Quisque gravida lorem leo, non rutrum enim hendrerit at.
                    <br />
                    Nullam dapibus tempor sem quis dictum. Etiam egestas ex sit amet arcu iaculis efficitur. Curabitur aliquet venenatis erat, vel lobortis orci mattis eget. Etiam et neque in ipsum efficitur cursus. Nulla posuere velit sed metus congue, a suscipit elit ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc lacus eros, dignissim eu lectus interdum, dictum accumsan nunc. Etiam posuere, metus ac commodo mattis, eros massa maximus felis, a imperdiet nunc eros eget ipsum.
                    <br />
                    Aliquam nibh nisi, facilisis sed commodo et, volutpat quis dolor. Praesent molestie quam non ex fringilla consectetur vel et sem. Vivamus in erat vel tellus venenatis pharetra. Nulla urna augue, elementum ut mattis at, tincidunt id sapien. Nam rhoncus velit ac risus rhoncus, dapibus fringilla augue facilisis. Morbi sed neque non urna lobortis mattis et sed nisl. Nunc maximus mauris a leo hendrerit commodo. Quisque sed lacus non enim hendrerit egestas vitae vitae dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ullamcorper eleifend leo. Mauris finibus diam ac nibh interdum, at placerat purus tincidunt. Aliquam consequat feugiat erat vel tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In id quam quis ipsum aliquam iaculis id ac ipsum. Vivamus hendrerit tortor massa, at rhoncus tortor volutpat vitae.
                    <br />
                    Nam ut tortor molestie, euismod orci eget, placerat magna. Phasellus vitae pharetra est. Nulla sem orci, volutpat at elit ac, lacinia facilisis turpis. Vivamus pellentesque nunc id lectus porttitor, at vestibulum nisl ultrices. Vivamus et nisl eu arcu blandit mollis lobortis id eros. Nulla sed congue lacus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut sollicitudin metus vestibulum, tincidunt libero et, euismod nunc. Nullam in malesuada urna, at cursus leo. Proin dui purus, luctus ut lobortis a, iaculis eget elit. Vivamus dapibus ultricies ultrices. Mauris rhoncus velit dolor, vel consequat mauris dictum eu.
                </label>
            </div>
        );
    }
}
