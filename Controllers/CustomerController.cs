﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ThynkSoftwareTechnicalTask.Database;
using ThynkSoftwareTechnicalTask.Repositories;

namespace ThynkSoftwareTechnicalTask.Controllers
{
    [ApiController]
    public class CustomerController : ControllerBase
    {
        public IRepository<Customer> _repository;
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(ILogger<CustomerController> logger, IRepository<Customer> repository)
        {
            _repository = repository;
            _logger = logger;
        }

        /// <summary>
        /// Add customer details
        /// </summary>
        [HttpPost]
        [Route("/api/addcustomer")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult AddCustomer(IFormFile image, [FromForm] string name, [FromForm] string position, [FromForm] string hobbies, [FromForm] string hometown, [FromForm] string motto, [FromForm] string description, [FromForm] string personalWebsite)
        {
            Customer customer = new Customer
            {
                Name = name,
                Position = position,
                Hobbies = hobbies,
                Hometown = hometown,
                Motto = motto,
                Description = description,
                PersonalWebsite = personalWebsite
            };

            if (image != null)
            {
                if (image.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        image.CopyTo(ms);
                        customer.Image = ms.ToArray();
                    }
                }
            }

            _repository.Insert(customer);

            return Ok(customer);
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        [HttpGet]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<List<Customer>> GetCustomers()
        {
            try
            {
                return _repository.GetAll().ToList();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates customer details
        /// </summary>
        [HttpPut]
        [Route("/api/customer/{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<Customer> UpdateCustomer([FromForm] int id, [FromForm] string name, [FromForm] string position, [FromForm] string hobbies, [FromForm] string hometown, [FromForm] string motto, [FromForm] string description, [FromForm] string personalWebsite)
        {
            try
            {
                Customer customer = _repository.GetById(id);
                customer.Name = name;
                customer.Position = position;
                customer.Hobbies = hobbies;
                customer.Hometown = hometown;
                customer.Motto = motto;
                customer.Description = description;
                customer.PersonalWebsite = personalWebsite;

                _repository.Update(customer);
                return customer;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes customer by ID
        /// </summary>
        [HttpDelete("{id}")]
        [Route("/api/customer/{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<int> DeleteCustomer(int id)
        {
            try
            {
                _repository.Delete(id);
                return id;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets information about specific customer
        /// </summary>
        [HttpGet]
        [Route("/api/customer/{id}")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<Customer> GetCustomer(int id)
        {
            try
            {
                return _repository.GetById(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
