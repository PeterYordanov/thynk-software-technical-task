﻿using System.Collections.Generic;
using ThynkSoftwareTechnicalTask.Database;

namespace ThynkSoftwareTechnicalTask.Repositories
{
    public  interface IRepository<T> where T : Customer
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(int id);
    }

}
